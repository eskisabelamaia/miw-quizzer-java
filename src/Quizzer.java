import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;


public class Quizzer {

	public static void main(String[] args) throws IOException {
		if (args.length <1 || args.length > 2) {
			System.out.println("Invalid command");
			System.exit(-1);
		} else if (args.length == 1) {
			manifestFile(args);
		} else if(args.length == 2) {
			quizzAssessmentFiles(args);
		}
	}
	
	public static void manifestFile(String[] args) throws MalformedURLException, IOException {
		Deserialization deserialization = new Deserialization();
		String strManifest = deserialization.readFile(args[0].toString());
		HashMap<Integer, HashMap<String, String>> tests = deserialization.parseManifestJson(strManifest);
		int counter = 1;
		for(int i=0;i<tests.size();i++) {
			HashMap<String, String> strURL = tests.get(i);
			String strQuizz = deserialization.readFileFromURL(strURL.get("quizz".concat(Integer.toString(i))));
			String strAssessment = deserialization.readFileFromURL(strURL.get("assessment".concat(Integer.toString(i))));
			String strScore = deserialization.readFileFromURL(strURL.get("scores".concat(Integer.toString(i))));
			Quizz quizz = new Quizz(deserialization.parseQuizzJson(strQuizz));
			Assessment assessment = new Assessment(deserialization.parseAssessmentJson(strAssessment));
			Scores scoreURL = new Scores(deserialization.parseScoresJson(strScore));
			Evaluator evaluator = new Evaluator(quizz.getQuestions(), assessment.getAssessment());
			Scores calculatedScores = new Scores(evaluator.evaluate());
			System.out.println("Scores: "+counter);
			if(scoreURL.getScores().keySet().equals(calculatedScores.getScores().keySet())) {
				System.out.println("Calculated scores are equal");
			} else {
				System.out.println("Calculated scores are not equal");
			}
			Statistics statistics = new Statistics(evaluator.getAnswersStatistics());
			statistics.writeStatistics(counter);
			System.out.println("Calculated statistics have been writen in the path files/statistics"+ counter +".json");
			counter++;
		}
	}
	
	public static void quizzAssessmentFiles(String[] args) throws IOException {
		Deserialization deserialization = new Deserialization();
		String strQuizz = deserialization.readFile(args[0].toString());
		String strAssessment = deserialization.readFile(args[1].toString());
		Quizz quizz = new Quizz(deserialization.parseQuizzJson(strQuizz));
		Assessment assessment = new Assessment(deserialization.parseAssessmentJson(strAssessment));
		Evaluator evaluator = new Evaluator(quizz.getQuestions(), assessment.getAssessment());
		Scores scores = new Scores(evaluator.evaluate());
		scores.writeScores();
		System.out.println("Calculated scores have been writen in the path files/scores.json");
		Statistics statistics = new Statistics(evaluator.getAnswersStatistics());
		statistics.writeStatistics(0);
		System.out.println("Calculated statistics have been writen in the path files/statistics.json");
	}
}
