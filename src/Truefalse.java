
public class Truefalse extends Question {
	private int id;
	private String questionText;
	private boolean correct;
	private double valueOK;
	private double valueFailed;
	private String feedback;
	
	public Truefalse(int id, String questionText) {
		super(id, questionText);
	}
	@Override
	public int getId() {
		return id;
	}
	@Override
	public void setId(int id) {
		this.id = id;
	}
	public String getQuestionText() {
		return questionText;
	}
	public void setQuestionText(String questionText) {
		this.questionText = questionText;
	}
	public boolean isCorrect() {
		return correct;
	}
	public void setCorrect(boolean correct) {
		this.correct = correct;
	}
	public double getValueOK() {
		return valueOK;
	}
	public void setValueOK(double valueOK) {
		this.valueOK = valueOK;
	}
	public double getValueFailed() {
		return valueFailed;
	}
	public void setValueFailed(double valueFailed) {
		this.valueFailed = valueFailed;
	}
	public String getFeedback() {
		return feedback;
	}
	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}
	
	@Override
	public double getStudentAnswerScore(Answer answer) {
		if (Boolean.parseBoolean(answer.getValue()) == correct) {
			return valueOK;
		} else {
			return valueFailed;
		}
	}
	
}
