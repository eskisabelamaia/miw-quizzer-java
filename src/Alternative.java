
public class Alternative {
	private int code;
	private String text;
	private double value;
	
	public Alternative(int code, String text, double value) {
		super();
		this.code = code;
		this.text = text;
		this.value = value;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
}
