import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;


public class Statistics {
	private HashMap<Integer,Integer> statistics;

	public Statistics(HashMap<Integer, Integer> statistics) {
		super();
		this.statistics = statistics;
	}

	public HashMap<Integer, Integer> getStatistics() {
		return statistics;
	}

	public void setStatistics(HashMap<Integer, Integer> statistics) {
		this.statistics = statistics;
	}

	public void writeStatistics(int fileCounter) throws IOException {
		int counter = 0;
		File newTextFile;
		
		String json = "{\"statistics\":[";
		for (int questionId : statistics.keySet()) {
			json += "{\"questionId\": " + questionId + ", \"quantity\": " + statistics.get(questionId) + "}";
			if(counter != statistics.size()-1) {
				json += ", ";
			}
			counter++;
		}
		json += "]}";
		if(fileCounter == 0) {
			newTextFile = new File("files/statistics.json");
		} else {
			newTextFile = new File("files/statistics"+fileCounter+".json");
		}
		FileWriter fileWriter = new FileWriter(newTextFile);
        fileWriter.write(json);
        fileWriter.close();
	}
}
