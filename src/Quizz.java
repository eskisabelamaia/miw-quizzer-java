import java.util.HashMap;


public class Quizz {
	private HashMap<Integer,Question> questions;

	public Quizz(HashMap<Integer, Question> questions) {
		super();
		this.questions = questions;
	}

	public HashMap<Integer, Question> getQuestions() {
		return questions;
	}

	public void setQuestions(HashMap<Integer, Question> questions) {
		this.questions = questions;
	}
}
