import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;


public class Scores {
	private HashMap<Integer,Score> scores;

	public Scores(HashMap<Integer, Score> scores) {
		super();
		this.scores = scores;
	}

	public HashMap<Integer, Score> getScores() {
		return scores;
	}

	public void setScores(HashMap<Integer, Score> scores) {
		this.scores = scores;
	}

	public void writeScores() throws IOException {
		int counter = 0;
		String json = "{\"scores\":[";
		for (int studentId : scores.keySet()) {
			json += "{\"studentId\": " + studentId + ", \"value\": " + scores.get(studentId).getValue() + "}";
			if(counter != scores.size()-1) {
				json += ", ";
			}
			counter++;
		}
		json += "]}";
		File newTextFile = new File("files/scores.json");
		FileWriter fileWriter = new FileWriter(newTextFile);
        fileWriter.write(json);
        fileWriter.close();
	}
}
