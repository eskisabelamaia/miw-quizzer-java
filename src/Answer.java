
public class Answer {
	private int question;
	private String value;
	
	public Answer(int question, String value) {
		super();
		this.question = question;
		this.value = value;
	}
	public int getQuestion() {
		return question;
	}
	public void setQuestion(int question) {
		this.question = question;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
