import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

public class Deserialization {

	public Deserialization() {
		super();
	}

	public String readFile (String file) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(file));
		String strJSON = "";
	    try {
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();

	        while (line != null) {
	            sb.append(line);
	            sb.append(System.lineSeparator());
	            line = br.readLine();
	        }
	        strJSON = sb.toString();
	    } finally {
	        br.close();
	    }
		return strJSON;
	}
	
	public String readFileFromURL (String file) throws MalformedURLException, IOException {
		InputStream is = new URL(file).openStream();
		BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();
	    int cp;
	    while ((cp = rd.read()) != -1) {
	      sb.append((char) cp);
	    }
	    return sb.toString();
		
	}
	
	public HashMap<Integer, Question> parseQuizzJson(String strJSON) {
		HashMap<Integer, Question> questions = new HashMap<Integer,Question>();
		HashMap<Integer, ArrayList<Alternative>> alternatives = new HashMap<Integer, ArrayList<Alternative>>();
		ArrayList<Alternative> alternative = null;

		JSONObject obj = new JSONObject(strJSON);

		JSONArray arr = obj.getJSONArray("questions");
		for (int i = 0; i < arr.length(); i++)
		{
			switch(arr.getJSONObject(i).getString("type")) {
			case "multichoice":
				Multichoice questionM = new Multichoice((arr.getJSONObject(i).getInt("id")), arr.getJSONObject(i).getString("questionText"));
				alternative = new ArrayList<Alternative>();
				for (int j = 0;j < arr.getJSONObject(i).getJSONArray("alternatives").length();j++) {
					alternative.add(new Alternative(arr.getJSONObject(i).getJSONArray("alternatives").getJSONObject(j).getInt("code"), arr.getJSONObject(i).getJSONArray("alternatives").getJSONObject(j).getString("text"), arr.getJSONObject(i).getJSONArray("alternatives").getJSONObject(j).getDouble("value")));
				}
				alternatives.put(arr.getJSONObject(i).getInt("id"), alternative);
				questionM.setAlternatives(alternatives);
				questions.put(arr.getJSONObject(i).getInt("id"), questionM);
				break;
			case "truefalse":
				Truefalse questionT = new Truefalse(arr.getJSONObject(i).getInt("id"), arr.getJSONObject(i).getString("questionText"));
				questionT.setCorrect(arr.getJSONObject(i).getBoolean("correct"));;
				questionT.setValueOK(arr.getJSONObject(i).getDouble("valueOK"));;
				questionT.setValueFailed(arr.getJSONObject(i).getDouble("valueFailed"));;
				questionT.setFeedback(arr.getJSONObject(i).getString("feedback"));
				questions.put(arr.getJSONObject(i).getInt("id"), questionT);
				break;
			}
		}
		return  questions;
	}
	
	public HashMap<Integer, Item> parseAssessmentJson(String strJSON) {
		HashMap<Integer, Item> items = new HashMap<Integer, Item>();
		Item item;
		HashMap<Integer, ArrayList<Answer>> answers;
		ArrayList<Answer> answer;
		JSONObject obj = new JSONObject(strJSON);

		JSONArray arr = obj.getJSONArray("items");
		
		for (int i = 0; i < arr.length(); i++) {
			answers = new HashMap<Integer, ArrayList<Answer>>();
			answer = new ArrayList<Answer>();
			for (int j = 0;j < arr.getJSONObject(i).getJSONArray("answers").length();j++) {
				if(arr.getJSONObject(i).getJSONArray("answers").getJSONObject(j).get("value").getClass() == Integer.class) {
					answer.add(new Answer(arr.getJSONObject(i).getJSONArray("answers").getJSONObject(j).getInt("question"), String.valueOf(arr.getJSONObject(i).getJSONArray("answers").getJSONObject(j).getInt("value"))));
					
				}else if (arr.getJSONObject(i).getJSONArray("answers").getJSONObject(j).get("value").getClass() == Boolean.class) {
					answer.add(new Answer(arr.getJSONObject(i).getJSONArray("answers").getJSONObject(j).getInt("question"), String.valueOf(arr.getJSONObject(i).getJSONArray("answers").getJSONObject(j).getBoolean("value"))));
				}
			}
			answers.put(arr.getJSONObject(i).getInt("studentId"), answer);
			item = new Item(arr.getJSONObject(i).getInt("studentId"), answers);
			items.put(arr.getJSONObject(i).getInt("studentId"), item);
		}
		return  items;
	}
	
	public HashMap<Integer, Score> parseScoresJson(String strJSON) {
		HashMap<Integer, Score> scores = new HashMap<Integer, Score>();
		Score score;
		JSONObject obj = new JSONObject(strJSON);
		JSONArray arr = obj.getJSONArray("scores");
		for (int i = 0; i < arr.length(); i++) {
			score = new Score(arr.getJSONObject(i).getInt("studentId"), arr.getJSONObject(i).getDouble("value"));
			scores.put(arr.getJSONObject(i).getInt("studentId"), score);
		}
		return  scores;
	}
	
	public HashMap<Integer, HashMap<String, String>> parseManifestJson(String strJSON) {
		HashMap<Integer, HashMap<String, String>> qtyTests = new HashMap<Integer, HashMap<String, String>>();
		HashMap<String, String> url = new HashMap<String, String>();
		JSONObject obj = new JSONObject(strJSON);

		JSONArray arr = obj.getJSONArray("tests");
		for (int i = 0; i < arr.length(); i++)
		{
		   url.put("quizz".concat(Integer.toString(i)), arr.getJSONObject(i).getString("quizz"));
		   url.put("assessment".concat(Integer.toString(i)), arr.getJSONObject(i).getString("assessment"));
		   url.put("scores".concat(Integer.toString(i)), arr.getJSONObject(i).getString("scores"));
		   qtyTests.put(i, url);
		}
		return  qtyTests;
	}
}
