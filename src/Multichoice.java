import java.util.ArrayList;
import java.util.HashMap;


public class Multichoice extends Question {

	private HashMap<Integer, ArrayList<Alternative>> alternatives;
		
	public Multichoice(int id, String questinText) {
		super(id, questinText);
	}
	
	public HashMap<Integer, ArrayList<Alternative>> getAlternatives() {
		return alternatives;
	}


	public void setAlternatives(
			HashMap<Integer, ArrayList<Alternative>> alternatives) {
		this.alternatives = alternatives;
	}

	@Override
	public double getStudentAnswerScore(Answer answer) {
		double score = 0.0;
        int alternativeCode = Integer.parseInt(answer.getValue());
        
        ArrayList<Alternative> alternative = alternatives.get(answer.getQuestion());

        for(int i=0;i<alternative.size();i++) {
        	if(alternative.get(i).getCode() == alternativeCode) {
        		score = alternative.get(i).getValue();
        	} 
        }
		return score;		
	}
}

