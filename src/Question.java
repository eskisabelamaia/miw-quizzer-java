
public abstract class Question {
	private int id;
	private String questionText;
	
	public Question(int id, String questionText) {
		super();
		this.id = id;
		this.questionText = questionText;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getQuestionText() {
		return questionText;
	}
	public void setQuestionText(String questinText) {
		this.questionText = questinText;
	}
	public abstract double getStudentAnswerScore(Answer answer);
}
