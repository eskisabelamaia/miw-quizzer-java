import java.util.ArrayList;
import java.util.HashMap;


public class Item {
	private int studentId;
	private HashMap<Integer, ArrayList<Answer>> answers;
	
	public Item(int studentId, HashMap<Integer, ArrayList<Answer>> answers) {
		super();
		this.studentId = studentId;
		this.answers = answers;
	}
	public int getStudentId() {
		return studentId;
	}
	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}
	public HashMap<Integer, ArrayList<Answer>> getAnswers() {
		return answers;
	}
	public void setAnswers(HashMap<Integer, ArrayList<Answer>> answers) {
		this.answers = answers;
	}
}
