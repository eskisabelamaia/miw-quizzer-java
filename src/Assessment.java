import java.util.HashMap;


public class Assessment {
	HashMap<Integer,Item> assessment;

	public Assessment(HashMap<Integer, Item> assessment) {
		super();
		this.assessment = assessment;
	}

	public HashMap<Integer, Item> getAssessment() {
		return assessment;
	}

	public void setAssessment(HashMap<Integer, Item> assessment) {
		this.assessment = assessment;
	}
}
