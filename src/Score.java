
public class Score {
	private int studentId;
	private double value;
	
	public Score(int studentId, double value) {
		super();
		this.studentId = studentId;
		this.value = value;
	}
	public int getStudentId() {
		return studentId;
	}
	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}
	public double getValue() {
		return value;
	}
	public void setValue(double value) {
		this.value = value;
	}
}
