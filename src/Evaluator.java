import java.util.ArrayList;
import java.util.HashMap;


public class Evaluator {

	private HashMap<Integer, Question> quizz;
	private HashMap<Integer, Item> assessment;
	
	public Evaluator(HashMap<Integer, Question> quizz,
			HashMap<Integer, Item> assessment) {
		super();
		this.quizz = quizz;
		this.assessment = assessment;
	}

	public HashMap<Integer,Score> evaluate () {
		HashMap<Integer,Score> scores = new HashMap<Integer,Score>();
		
		for (int studentId : assessment.keySet()) {
			if(assessment.containsKey(studentId)) {
				double grade = scoreCalculation(studentId, assessment.get(studentId).getAnswers().get(studentId));
				Score score = new Score(studentId, grade);
				scores.put(studentId, score);
			}
		}
		
		return scores;	
	}
	
	public double scoreCalculation(int studentId, ArrayList<Answer> answers) {
		double score = 0.0;
		for (int i=0;i<answers.size();i++) {
        	if (quizz.containsKey(answers.get(i).getQuestion())) {
        		score += quizz.get(answers.get(i).getQuestion()).getStudentAnswerScore(answers.get(i));
            }
		}
		return score;
	}
	
	public void compareScores()
	{
		
	}
	
	public HashMap<Integer,Integer> getAnswersStatistics() 
	{
		HashMap<Integer,Integer> statistics = new HashMap<Integer,Integer>();
		
		for (int studentId : assessment.keySet()) {
			if(assessment.containsKey(studentId)) {
				for (int i=0;i<assessment.get(studentId).getAnswers().get(studentId).size();i++) {
		        	if (quizz.containsKey(assessment.get(studentId).getAnswers().get(studentId).get(i).getQuestion())) {
		        		if (quizz.get(assessment.get(studentId).getAnswers().get(studentId).get(i).getQuestion()).getStudentAnswerScore(assessment.get(studentId).getAnswers().get(studentId).get(i)) > 0) {
		        			if (statistics.containsKey(assessment.get(studentId).getAnswers().get(studentId).get(i).getQuestion())) {
		        				statistics.replace(assessment.get(studentId).getAnswers().get(studentId).get(i).getQuestion(), statistics.get(assessment.get(studentId).getAnswers().get(studentId).get(i).getQuestion())+1);
		        			} else {
		        				statistics.put(assessment.get(studentId).getAnswers().get(studentId).get(i).getQuestion(), 1);
		        			}
		        		}
		            }
				}
			}
		}
			
		return statistics;
	}
}
