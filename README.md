# Quizzer - Java

Quizzer is an application to calculate tests scores. The application calculates the scores taking a Quizz file and an Assessment file or taking a Manifest file containing the URL of the previous files.
The format for all the files is JSON.

The Quizz file contains questions and has the following format:

      { "questions": 
         [ { "type": "multichoice", 
             "id" : 1,
         	   "questionText": "Scala fue creado por...",
      	   "alternatives": [ 
      	     { "text": "Martin Odersky",   "code": 1, "value": 1 },
      		 { "text": "James Gosling",    "code": 2, "value": -0.25 },
      		 { "text": "Guido van Rossum", "code": 3, "value": -0.25 }
      		]
      	 },
      	{ "type" : "truefalse",
      	  "id" : 2,
      	  "questionText": "El creador de Ruby es Yukihiro Matsumoto",
      	  "correct": true,
      	  "valueOK": 1,
      	  "valueFailed": -0.25,
      	  "feedback": "Yukihiro Matsumoto es el principal desarrollador de Ruby desde 1996" 
      	}
       ]
      }

The types of questions that supports the application are Multichoice and TrueFalse.

The Assessment file has the answers of the students of an evaluation and has the following format:

      { "items": 
       [ { "studentId": 234 ,
          "answers": 
          [ { "question" : 1, "value": 1 },
            { "question" : 2, "value": false }
          ] 
        },
        { "studentId": 245 ,      
          "answers": 
          [ { "question" : 1, "value": 1 },
            { "question" : 2, "value": true }
          ] 
        }, 
        { "studentId": 221 ,      
          "answers": 
          [ { "question" : 1, "value": 2 },
            { "question" : 2, "value": true }
          ] 
        }
       ] 
      }

The obtained scores have the following format:

    { "scores": 
     [ { "studentId": 234, "value": 0.75 } ,
       { "studentId": 245, "value": 2.0 } ,
       { "studentId": 221, "value": 0.75 }
     ]
    }

The Manifest file with test questions, answers and scores has the following format:

      { "tests": 
         [ { "type": "score", 
             "quizz" : "http://di002.edv.uniovi.es/~labra/cursos/MIW/POO/Curso1415/Ejercicios/quizz1.json",
         	   "assessment": "http://di002.edv.uniovi.es/~labra/cursos/MIW/POO/Curso1415/Ejercicios/assessment1.json",
      	   "scores": "http://di002.edv.uniovi.es/~labra/cursos/MIW/POO/Curso1415/Ejercicios/scores11.json"
      	 },
      	 { "type": "score", 
             "quizz" : "http://di002.edv.uniovi.es/~labra/cursos/MIW/POO/Curso1415/Ejercicios/quizz1.json",
         	   "assessment": "http://di002.edv.uniovi.es/~labra/cursos/MIW/POO/Curso1415/Ejercicios/assessment2.json",
      	   "scores": "http://di002.edv.uniovi.es/~labra/cursos/MIW/POO/Curso1415/Ejercicios/scores12.json"
      	 }
        ]
      }
      
Using the Manifest file the application will verify that generated scores matches the scores declared in the Manifest file.

The application also calculates statistics for each question.

The application has been made following a test base development in the following languages:

    - Ruby
    - PHP
    - Python
    - Scala
    - Java

The application can be executed using the command line accessing the directory miw-quizzer-java and executing:

    - java -jar Quizzer.jar files/manifest.json
    - java -jar Quizzer.jar files/quizz.json files/assessment.json
